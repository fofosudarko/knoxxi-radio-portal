// collection.js

function isCollection(collection) {
	return Array.isArray(collection);
}

export default class Collection {
	static prepend(collection = [], item = {}) {
		if (!isCollection(collection)) {
			return [];
		}
		return [item, ...collection];
	}

	static prependAll(collection = [], items = [], isObject = true) {
		if (!isCollection(collection)) {
			return [];
		}
		let list;
		if (isObject) {
			const difference = Collection.findDifference(items, collection);
			list = [...difference, ...collection];
		} else {
			list = [...collection, ...items];
		}
		return list;
	}

	static append(collection = [], item = {}) {
		if (!isCollection(collection)) {
			return [];
		}
		return [...collection, item];
	}

	static appendAll(collection = [], items = [], isObject = true) {
		if (!isCollection(collection)) {
			return items;
		}
		let list;
		if (isObject) {
			const difference = Collection.findDifference(items, collection);
			list = [...collection, ...difference];
		} else {
			list = [...collection, ...items];
		}
		return list;
	}

	static update(collection = [], item = {}, property = 'id') {
		if (!isCollection(collection)) {
			return [];
		}
		const itemIndex = collection.findIndex((i) => i[property] === item[property]);
		if (itemIndex === -1) {
			return collection;
		}
		if (Object.isFrozen(collection)) {
			collection = Array.from(collection);
		}
		collection.splice(itemIndex, 1, item);
		return collection;
	}

	static updateAll(collection = [], items = [], property = 'id') {
		if (!isCollection(collection)) {
			return [];
		}
		if (Object.isFrozen(collection)) {
			collection = Array.from(collection);
		}
		for (const item of items) {
			const itemIndex = collection.findIndex((i) => i[property] === item[property]);
			if (itemIndex === -1) {
				continue;
			}
			collection.splice(itemIndex, 1, item);
		}
		return collection;
	}

	static list(collection = [], items = [], isObject = true) {
		return Collection.appendAll(collection, items, isObject);
	}

	static upsert(collection = [], item = {}, property = 'id', operation = 'PREPEND_ITEM') {
		if (!isCollection(collection)) {
			return [];
		}
		if (!['PREPEND_ITEM', 'APPEND_ITEM'].includes(operation)) {
			throw new Error('Operation unknown. Must be either PREPEND_ITEM or APPEND_ITEM');
		}
		const itemIndex = collection.findIndex((i) => i[property] === item[property]);
		if (itemIndex === -1) {
			if (operation === 'PREPEND_ITEM') {
				return Collection.prepend(collection, item);
			} else {
				return Collection.append(collection, item);
			}
		}
		if (Object.isFrozen(collection)) {
			collection = Array.from(collection);
		}
		collection.splice(itemIndex, 1, item);
		return collection;
	}

	static remove(collection = [], item = {}, property = 'id') {
		return isCollection(collection) && item
			? collection.filter((i) => i[property] !== item[property])
			: collection;
	}

	static removeAll(collection = [], items = [], property = 'id') {
		return Collection.findDifference(collection, items, property);
	}

	static findIntersection(firstCollection = null, secondCollection = null, property = 'id') {
		if (!isCollection(firstCollection) && !isCollection(secondCollection)) {
			return secondCollection;
		} else if (!isCollection(secondCollection) && isCollection(firstCollection)) {
			return firstCollection;
		}
		const firstCollectionSet = new Set(firstCollection.map((item) => item[property]));
		const secondCollectionSet = new Set(secondCollection.map((item) => item[property]));
		const intersection = [
			...[...firstCollectionSet].filter((item) => secondCollectionSet.has(item))
		];
		return firstCollection.filter((item) => intersection.includes(item[property]));
	}

	static findDifference(firstCollection = null, secondCollection = null, property = 'id') {
		if (!isCollection(firstCollection) && isCollection(secondCollection)) {
			return secondCollection;
		} else if (!isCollection(secondCollection) && isCollection(firstCollection)) {
			return firstCollection;
		}
		const firstCollectionSet = new Set(firstCollection.map((item) => item[property]));
		const secondCollectionSet = new Set(secondCollection.map((item) => item[property]));
		const difference = [
			...[...firstCollectionSet].filter((item) => !secondCollectionSet.has(item))
		];
		return firstCollection.filter((item) => difference.includes(item[property]));
	}

	static get(collection = [], item = {}, property = 'id') {
		if (!isCollection(collection)) {
			return item;
		}
		return collection.find((i) => i[property] === item[property]) ?? null;
	}
}
