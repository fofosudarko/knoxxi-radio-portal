export function generateQuerystring(params = null) {
	let queryString = '';
	if (typeof params !== 'object' || !params) {
		return queryString;
	}
	const queryParams = new URLSearchParams();
	for (const key of Object.keys(params)) {
		const value = params[key];
		if (value) {
			queryParams.set(key, value);
		}
	}
	if (queryParams.toString() !== '') {
		queryString = '?' + queryParams.toString();
	}
	return queryString;
}
