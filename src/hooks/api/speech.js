import { error } from '@sveltejs/kit';

import { SpeechService } from '$api/radio';
import {
	listSpeeches,
	addSpeech,
	updateSpeech,
	removeSpeech,
	setSpeechCreated,
	setSpeechUpdated,
	setSpeechRemoved,
	setSpeechEndPaging as setEndPaging
} from '$stores/speech';
import { setProcessing } from '$stores/processing';

export async function handleListSpeeches(options = null) {
	const { page, size } = options ?? {};
	try {
		const response = await SpeechService.listSpeeches(page, size);
		const data = await response.json();
		if (response.ok) {
			if (data.data?.length) {
				listSpeeches(data.data);
			} else {
				setEndPaging(true);
			}
		} else {
			throw error(response.status, data.error.message);
		}
	} catch (err) {
		console.error(err);
	}
}

export async function handleCreateSpeech(body) {
	setProcessing(true);
	try {
		const response = await SpeechService.createSpeech(body);
		const data = await response.json();
		if (response.ok) {
			addSpeech(data.data);
			setSpeechCreated(true);
		} else {
			throw error(response.status, data.error.message);
		}
	} catch (err) {
		console.error(err);
	}
	setProcessing(false);
}

export async function handleUpdateSpeech(speech, body) {
	setProcessing(true);
	try {
		const response = await SpeechService.updateSpeech(speech, body);
		const data = await response.json();
		if (response.ok) {
			updateSpeech(data.data);
			setSpeechUpdated(true);
		} else {
			throw error(response.status, data.error.message);
		}
	} catch (err) {
		console.error(err);
	}
	setProcessing(false);
}

export async function handleRemoveSpeech(speech) {
	setProcessing(true);
	try {
		const response = await SpeechService.removeSpeech(speech);
		const data = await response.json();
		if (response.ok) {
			if (data.data) {
				removeSpeech(speech);
				setSpeechRemoved(true);
			}
		} else {
			throw error(response.status, data.error.message);
		}
	} catch (err) {
		console.error(err);
	}
	setProcessing(false);
}
