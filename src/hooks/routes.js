import { goto } from '$app/navigation';
import { generateQuerystring } from '$utils';

const routes = {
	INDEX_ROUTE: '/',
	SPEECHES_ROUTE: '/speeches',
	SPEECH_NEW_ROUTE: '/speeches/new',
	SPEECH_EDIT_ROUTE: '/speeches/{{SPEECH_ID}}/edit'
};

export default function useRoutes() {
	const useIndexRoute = () => {
		const indexRoute = routes.INDEX_ROUTE;

		const handleIndexRoute = async (options = {}) => {
			const { query = null } = options;
			await goto(`${indexRoute}${generateQuerystring(query)}`);
		};

		return { indexRoute, handleIndexRoute };
	};

	const useSpeechesRoute = () => {
		const speechesRoute = handleNamedRoute({
			route: routes.SPEECHES_ROUTE
		});

		const handleSpeechesRoute = async (options = {}) => {
			const { query = null } = options;
			await goto(`${speechesRoute}${generateQuerystring(query)}`);
		};

		return { speechesRoute, handleSpeechesRoute };
	};

	const useSpeechNewRoute = () => {
		const speechNewRoute = handleNamedRoute({
			route: routes.SPEECH_NEW_ROUTE
		});

		const handleSpeechNewRoute = async (options = {}) => {
			const { query = null } = options;
			await goto(`${speechNewRoute}${generateQuerystring(query)}`);
		};

		return { speechNewRoute, handleSpeechNewRoute };
	};

	const useSpeechEditRoute = (speech = null) => {
		const speechEditRoute = handleNamedRoute({
			route: routes.SPEECH_EDIT_ROUTE,
			speech
		});

		const handleSpeechEditRoute = async (options = {}) => {
			const { query = null } = options;
			await goto(`${speechEditRoute}${generateQuerystring(query)}`);
		};

		return { speechEditRoute, handleSpeechEditRoute };
	};

	return {
		goto,
		useIndexRoute,
		useSpeechesRoute,
		useSpeechNewRoute,
		useSpeechEditRoute,
		routes
	};
}

function handleNamedRoute({ route, speech = null }) {
	route = route || '';
	const speechId = speech?.id;

	if (speechId) {
		route = route.replace('{{SPEECH_ID}}', speechId);
	}

	return route;
}
