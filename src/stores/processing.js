import { writable } from 'svelte/store';

export const processing = writable(false);

export function setProcessing(value) {
	processing.set(value);
}
