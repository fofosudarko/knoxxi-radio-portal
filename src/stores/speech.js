import { writable } from 'svelte/store';

import Collection from '$utils/collection';

export const speeches = writable(null);
export const speech = writable(null);
export const speechPage = writable(null);
export const speechEndPaging = writable(false);
export const speechCreated = writable(false);
export const speechUpdated = writable(false);
export const speechRemoved = writable(false);

export function listSpeeches(_speeches = []) {
	speeches.update((data) => Collection.list(data ?? [], _speeches));
}

export function addSpeech(speech) {
	speeches.update((data) => Collection.prepend(data ?? [], speech));
}

export function updateSpeech(speech) {
	speeches.update((data) => Collection.update(data ?? [], speech));
}

export function removeSpeech(speech) {
	speeches.update((data) => Collection.remove(data ?? [], speech));
}

export function setSpeech(_speech) {
	speech.set(_speech);
}

export function clearSpeeches() {
	speeches.set(null);
	speechPage.set(1);
	speechEndPaging.set(false);
}

export function setSpeechPage(page) {
	speechPage.set(page);
}

export function setSpeechEndPaging(endPaging) {
	speechEndPaging.set(endPaging);
}

export function setSpeechCreated(_speechCreated) {
	speechCreated.set(_speechCreated);
}

export function setSpeechUpdated(_speechUpdated) {
	speechUpdated.set(_speechUpdated);
}

export function setSpeechRemoved(_speechRemoved) {
	speechRemoved.set(_speechRemoved);
}
