import { fetchRadioResource } from '../fetch/radio';
import { generateQuerystring } from '../../utils';
import { DEFAULT_PAGE, DEFAULT_PAGE_SIZE } from '../../config';

export default class SpeechService {
	static async createSpeech(body = null) {
		return await fetchRadioResource('/speeches', {
			method: 'POST',
			body: JSON.stringify(body)
		});
	}

	static async removeSpeech(speech = null) {
		const speechId = speech?.id;
		return await fetchRadioResource(`/speeches/${speechId}`, {
			method: 'DELETE'
		});
	}

	static async updateSpeech(speech = null, body = null) {
		const speechId = speech?.id;
		return await fetchRadioResource(`/speeches/${speechId}`, {
			method: 'PUT',
			body: JSON.stringify(body)
		});
	}

	static async listSpeeches(page = DEFAULT_PAGE, size = DEFAULT_PAGE_SIZE) {
		return await fetchRadioResource(`/speeches/${generateQuerystring({ page, size })}`, {
			method: 'GET'
		});
	}

	static async getSpeech(speech = null) {
		const speechId = speech?.id;
		return await fetchRadioResource(`/speeches/${speechId}`, {
			method: 'GET'
		});
	}
}
