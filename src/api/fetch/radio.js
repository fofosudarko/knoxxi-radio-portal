import {
	PUBLIC_KNOXXI_RADIO_SERVICE_API_URL,
	PUBLIC_USER_AUTHENTICATION_TOKEN
} from '$env/static/public';

const headers = new Headers();

// fetch resources from radio service api
export async function fetchRadioResource(resource, options = null) {
	const url = PUBLIC_KNOXXI_RADIO_SERVICE_API_URL + resource;
	headers.set('Authorization', `Bearer ${PUBLIC_USER_AUTHENTICATION_TOKEN}`);
	headers.set('Content-Type', 'application/json');
	if (options?.headers) {
		const optionsHeaders = options?.headers ?? null;
		for (const key of optionsHeaders.keys()) {
			headers.append(key, optionsHeaders.get(key));
		}
		delete options?.headers;
	}
	return await fetch(url, { ...options, headers });
}
