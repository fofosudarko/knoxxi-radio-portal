import * as yup from 'yup';

const NAME_INPUT_VALIDATION = {
	string: {
		errorMessage: 'Name must be a string'
	},
	required: {
		errorMessage: 'Name required'
	}
};

const TITLE_INPUT_VALIDATION = {
	string: {
		errorMessage: 'Title must be a string'
	},
	required: {
		errorMessage: 'Title required'
	}
};

const TEXT_INPUT_VALIDATION = {
	string: {
		errorMessage: 'Text must be a string'
	},
	required: {
		errorMessage: 'Text required'
	},
	maxLength: {
		value: 3000,
		errorMessage: 'Text length exceeded 3000 characters'
	}
};

export const YUP_NAME_VALIDATOR = yup
	.string(NAME_INPUT_VALIDATION.string.errorMessage)
	.required(NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_TITLE_VALIDATOR = yup
	.string(TITLE_INPUT_VALIDATION.string.errorMessage)
	.required(TITLE_INPUT_VALIDATION.required.errorMessage);

export const YUP_TEXT_VALIDATOR = yup
	.string(TEXT_INPUT_VALIDATION.string.errorMessage)
	.required(TEXT_INPUT_VALIDATION.required.errorMessage)
	.max(TEXT_INPUT_VALIDATION.maxLength.value, TEXT_INPUT_VALIDATION.maxLength.errorMessage);
