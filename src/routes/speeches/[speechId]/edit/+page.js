import { error } from '@sveltejs/kit';

import { SpeechService } from '$api/radio';
import { setSpeech } from '$stores/speech';

export async function load({ params }) {
	const response = await SpeechService.getSpeech({ id: params?.speechId });
	const data = await response.json();
	if (response.ok) {
		setSpeech(data.data);
		return data.data;
	} else {
		throw error(response.status, data.error.message);
	}
}
