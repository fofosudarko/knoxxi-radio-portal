import { SpeechService } from '$api/radio';
import {
	listSpeeches,
	setSpeechEndPaging as setEndPaging,
	setSpeechPage as setPage
} from '$stores/speech';
import { error } from '@sveltejs/kit';

export async function load({ url }) {
	const page = parseInt(url.searchParams.get('page'));
	const response = await SpeechService.listSpeeches(page);
	const data = await response.json();
	if (response.ok) {
		if (data.data?.length) {
			listSpeeches(data.data);
			setPage(page);
			return { speeches: data.data };
		} else {
			setEndPaging(true);
		}
	} else {
		throw error(response.status, data.error.message);
	}
	return null;
}
