export { default as SpeechInput } from './SpeechInput/SpeechInput.svelte';
export { default as SpeechEditRoute } from './SpeechEdit/SpeechEditRoute.svelte';
export { default as SpeechEditPage } from './SpeechEditPage/SpeechEditPage.svelte';
export { default as SpeechNewRoute } from './SpeechNew/SpeechNewRoute.svelte';
export { default as SpeechNewPage } from './SpeechNewPage/SpeechNewPage.svelte';
export { default as SpeechRemove } from './SpeechRemove/SpeechRemove.svelte';
export { default as SpeechesPage } from './SpeechesPage/SpeechesPage.svelte';
export { default as SpeechesListRoute } from './SpeechesList/SpeechesListRoute.svelte';
